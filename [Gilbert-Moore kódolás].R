# P:    bemeneti eloszlás (pontosabban a sűrűségfüggvény diszkrét értékei: P_i)
# K:    kódabc/csatornaabc karakterei

remove(list = ls())

# input
#P = c(3/8, 1/8, 1/6, 1/8, 1/12, 1/8)
#K = c('0', '1', '2')

P = c(0.2, 0.18, 0.1, 0.1, 0.1, 0.061, 0.059, 0.04, 0.04, 0.04, 0.04, 0.03, 0.01)
K = c('0', '1', '2')



#### calculations
X = rep(0, length(P));
for(i in 1 : (length(P) - 1)) {
  X[(i + 1)] = X[i] + P[i];
}
for(i in 1 : length(P)) {
  X[i] = X[i] + P[i] / 2;
}


queue.create <- function() {
  qenv <- new.env();
  queue <- list()
  assign('queue', queue, envir = qenv)
  
  return(qenv)
}

queue.delete <- function(qenv) {
  queue = get('queue', envir = qenv)
  remove(queue, envir = qenv)
}

queue.enqueue <- function(qenv, obj) {
  queue = get('queue', envir = qenv)
  queue[[length(queue) + 1]] <- obj;
  assign('queue', queue, envir = qenv)
}

queue.dequeue <- function(qenv) {
  queue = get('queue', envir = qenv)
  
  if(length(queue) > 0) {
    obj = queue[[1]]
    queue[[1]] <- NULL
    
    assign('queue', queue, envir = qenv)
    
    return(obj)
  }
  
  return(NULL)
}

queue.size <- function(qenv) {
  queue = get('queue', envir = qenv)
  
  return(length(queue))
}


QUEUE = queue.create()
queue.enqueue(QUEUE, list(
  "code" = "",
  "start" = 0.0,
  "end" = 1.0,
  "values" = c(X),
  "Pvalues" = c(P)
))

CODES <- list();

while(queue.size(QUEUE) > 0) {
  obj = queue.dequeue(QUEUE)
  
  if(length( obj[['values']] ) > 1) {
    #split region into equal length(K) sized parts
    
    Ksize = length(K)
    for(i in 1 : Ksize) {
      v = obj[["values"]]
      pv = obj[["Pvalues"]]
      regionDif = (obj[["end"]] - obj[["start"]]);
      
      code = paste(obj[["code"]], K[i], sep = "")
      start = obj[["start"]] + (i-1) / Ksize * regionDif 
      end = obj[["start"]] + (i) / Ksize * regionDif
      values = v[v >= start & v < end];
      Pvalues = pv[v >= start & v < end];
      
      queue.enqueue(QUEUE, list(
        "code" = code,
        "start" = start,
        "end" = end,
        "values" = values,
        "Pvalues" = Pvalues
      ))
    } 
  } else if (length( obj[['values']] ) == 1) {
    CODES[[length(CODES) + 1]] = list(
      "P_i" = obj[["Pvalues"]][1],
      "X_i" = obj[["values"]][1],
      "code" = obj[["code"]]
    );
  }
  
}




##output

CODES.P_i = rep(0, length(CODES));
CODES.X_i = rep(0, length(CODES));
CODES.code = rep('', length(CODES));
for(i in 1 :  length(CODES)) {
  CODES.P_i[i] = CODES[[i]][['P_i']]
  CODES.X_i[i] = CODES[[i]][['X_i']]
  CODES.code[i] = CODES[[i]][['code']]
}

CODES = data.frame(
  P_i = CODES.P_i,
  X_i = CODES.X_i,
  code = CODES.code,
  L_i = nchar(as.character(CODES.code)),
  T_i = nchar(as.character(CODES.code))
)

CODES= CODES[order(CODES$X_i),]

L_i = CODES$L_i
T_i = CODES$T_i

entropiaP = 0;
L_atlagosKodhossz_L = 0; # L
Kappa_varhatoKoltseg_Kappa = 0;
for (i in 1:length(P)) {
  entropiaP = entropiaP + (-1) * P[i] * log(P[i], base = 2);
  L_atlagosKodhossz_L = L_atlagosKodhossz_L + P[i] * L_i[i];
  Kappa_varhatoKoltseg_Kappa = Kappa_varhatoKoltseg_Kappa + P[i] * T_i[i];
}
Gamma_hatasfok_Gamma = entropiaP /(L_atlagosKodhossz_L * log(length(K), base = 2))

Tau_egyBetuVarhatoAtviteliIdejeVagyKoltsege_Tau = 1

queue.delete(QUEUE)
remove(QUEUE, obj, CODES.code, CODES.P_i, L_i, pv, Pvalues, T_i, CODES.X_i, i, code, end, Ksize, regionDif, start, v, values, queue.create, queue.delete, queue.dequeue, queue.enqueue, queue.size)

