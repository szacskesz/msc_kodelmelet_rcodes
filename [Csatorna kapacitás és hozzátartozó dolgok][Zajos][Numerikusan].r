#

remove(list = ls())

#input
T = matrix(c(1/2, 1/3, 1/6, 0, 0, 1,
             1/6, 1/2, 1/3, 0, 1, 0,
             1/3, 1/6, 1/2, 1, 0, 0),
           nrow = 3, byrow = TRUE)




#calculations
Dj = function(T, Q, j) {
  sum = 0;
  
  for (i in length(Q)) {
    if(T[i,j] != 0) {
      sum = sum + T[i,j] * log(T[i,j] / Q[i])
    }
  }
  
  return(sum);
}

nRow = dim(T)[1]
nCol = dim(T)[2]



P_N = rep(1/nCol, nCol);
S_N = 0;



TP_N = T %*% P_N;

S_N = 0;
for(j in 1:nCol) {
  S_N = S_N + P_N[j] * exp( Dj(T, TP_N, j) )
}

for(j in 1:nCol) {
  P_N[j] = ( P_N[j] * exp( Dj(T, TP_N, j) ) )/  S_N 
}





for(i in 1:100) {
  TP_N = T %*% P_N;
  
  S_N = 0;
  for(j in 1:nCol) {
    S_N = S_N + P_N[j] * exp( Dj(T, TP_N, j) )
  }
  
  for(j in 1:nCol) {
    P_N[j] = ( P_N[j] * exp( Dj(T, TP_N, j) ) )/  S_N 
  }
}

P_N
S_N


#remove(nCol, nRow)
