
remove(list = ls());

#### input 
K = c('1', '2', '3');
#P = c(0.385, 0.179, 0.154, 0.154, 0.128);
#P = c(20, 40, 10, 8, 7, 15);
P = c(0.31, 0.15, 0.18, 0.07, 0.08, 0.09, 0.12);
K = c('0', '1', '2', '3');
P = c(0.36, 0.17, 0.09, 0.09, 0.07, 0.06, 0.04, 0.04, 0.03, 0.03, 0.02);




#### calculations
P = P / sum(P); #normalize data, to have probabilities

TREE = list();

for(i in 1 : length(P)) {
  TREE[[length(TREE) + 1]] = list(
    P=P[i],
    childs=rep(NULL, length(K))
  );
}

k = length(K) - ( (1 - length(P)) %% (length(K)-1) ) 

while(length(TREE) > 1) {
  sumP = 0;
  smallest_array = c();
  for(i in 1: length(K)) {
    if(i <= k && length(TREE) > 0) {
      smallest_value = Inf;
      smallest_index = Inf; 
      for(j in 1 : length(TREE)) {
        if(TREE[[j]][['P']] < smallest_value) {
          smallest_value = TREE[[j]][['P']];
          smallest_index = j;
        }
      }
      
      smallest = TREE[[smallest_index]];
      TREE[[smallest_index]] = NULL;
      
      sumP = sumP + smallest[['P']];
    } else {
      smallest = list(
        P = NULL,
        childs=rep(NULL, length(K))
      );
    }
    
    smallest_array[[i]] = smallest;
  }
  

  
  TREE[[length(TREE) + 1]] = list(
    P=sumP,
    childs=smallest_array
  );
  
  k = length(K); # többi lépésben már lehet s = length(K) darabot kiválasztani
}

iterateTree = function(CODES, node, code) {
  print(paste(code, K[1], sep = ""))
  if(is.null(node[['childs']])) {
    if(!is.null(node[['P']])) {
      CODES[[length(CODES) + 1]] = list(
        P_i = node[['P']],
        code = code
      );
    }
  } else {
    for(i in 1:length(K)) {
      if(!is.null(node[['childs']][i])) {
        CODES = iterateTree(CODES, node[['childs']][[i]], paste(code, K[i], sep = ""))
      }
    }
  }
  
  return(CODES)
}

CODES = iterateTree(list(), TREE[[1]], '')




##output
CODES.P_i = rep(0, length(CODES));
CODES.code = rep('', length(CODES));
for(i in 1 :  length(CODES)) {
  CODES.P_i[i] = CODES[[i]][['P_i']]
  CODES.code[i] = CODES[[i]][['code']]
}

CODES = data.frame(
  P_i = CODES.P_i,
  code = CODES.code,
  L_i = nchar(as.character(CODES.code)),
  T_i = nchar(as.character(CODES.code))
)

CODES= CODES[order(CODES$P_i),]

#forrásabc költségei/továbbitási idejei
L_i = CODES$L_i
T_i = CODES$T_i
P_i = CODES$P_i

entropiaP = 0;
L_atlagosKodhossz_L = 0; # L
Kappa_varhatoKoltseg_Kappa = 0;
for (i in 1:length(P)) {
  entropiaP = entropiaP + (-1) * P[i] * log(P[i], base = 2);
  L_atlagosKodhossz_L = L_atlagosKodhossz_L + P_i[i] * L_i[i];
  Kappa_varhatoKoltseg_Kappa = Kappa_varhatoKoltseg_Kappa + P_i[i] * T_i[i];
}
Gamma_hatasfok_Gamma = entropiaP /(L_atlagosKodhossz_L * log(length(K), base = 2))

Tau_egyBetuVarhatoAtviteliIdejeVagyKoltsege_Tau = 1


remove(smallest, k, P_i, L_i, T_i, smallest_array, smallest_index, smallest_value, i, j, sumP, iterateTree, CODES.code, CODES.P_i)
