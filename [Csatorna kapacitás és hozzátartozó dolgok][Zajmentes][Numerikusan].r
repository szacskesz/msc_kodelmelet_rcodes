# C:    csatornakapacitás
# T:    csatornaabc betűinek átviteli ideje
# v:    információtovábbítás sebessége
# u:    = v * ln 2  (a könnyebb számítások végett)
# Q:    csatornaabc betűinek optimális eloszlása
# tau:  egy (csatornaabc) betűre jutó várható átviteli idő

remove(list = ls())

#input
T=c(0.14, 1.14, 0.7)
#T=c(0.1, 0.8, 0.9, 2.0)




#calculations
TABLE = list();

u=0
for (i in 1:100) { #iterations count
  g_Uk = sum(exp(-u*T));
  minus_derivated_g_Uk = sum(T*exp(-u*T));
  newU = u + (g_Uk * log(g_Uk) ) / (minus_derivated_g_Uk);
  
  TABLE[[length(TABLE) + 1]] = list(
    Uk=u, 
    g_Uk = g_Uk,
    minus_derivated_g_Uk = minus_derivated_g_Uk,
    delta_Uk = newU - u
  );

  u = newU;
}

TABLE.k = rep(0, length(TABLE));
TABLE.Uk = rep(0, length(TABLE));
TABLE.g_Uk = rep(0, length(TABLE));
TABLE.minus_derivated_g_Uk = rep(0, length(TABLE));
TABLE.delta_Uk = rep(0, length(TABLE));
for(i in 1 :  length(TABLE)) {
  TABLE.k[i] = i-1;
  TABLE.Uk[i] = TABLE[[i]][['Uk']];
  TABLE.g_Uk[i] = TABLE[[i]][['g_Uk']];
  TABLE.minus_derivated_g_Uk[i] = TABLE[[i]][['minus_derivated_g_Uk']];
  TABLE.delta_Uk[i] = TABLE[[i]][['delta_Uk']];
}

TABLE = data.frame(
  k = TABLE.k,
  Uk = TABLE.Uk,
  g_Uk = TABLE.g_Uk,
  minus_derivated_g_Uk = TABLE.minus_derivated_g_Uk,
  delta_Uk = TABLE.delta_Uk
)

v=u/log(2);
C = v; 

Q=rep(0, length(T));
for (i in 1:length(T)) {
  Q[i] = 2^(-1* C * T[i]);
}

tau = 0;
for (i in 1:length(T)) {
   tau = tau + Q[i] * T[i];
}

remove(i, u, v, TABLE.k, TABLE.Uk, TABLE.g_Uk, TABLE.minus_derivated_g_Uk, TABLE.delta_Uk, newU, g_Uk, minus_derivated_g_Uk)
